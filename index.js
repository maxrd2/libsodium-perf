const _sodium = require('libsodium-wrappers');
(async() => {
	await _sodium.ready;
	const sodium = _sodium;

	const nIterations = 100;

	console.log('generate random data');
	const data = sodium.randombytes_buf(1024 * 1024);

	// https://libsodium.gitbook.io/doc/secret-key_cryptography/secretstream
	console.log('*** MEASURE SECRET KEY ENCRYPTION');

	console.log('generate secret key');
	const key = sodium.crypto_secretstream_xchacha20poly1305_keygen();

	console.log('init stream encryption');
	const res = sodium.crypto_secretstream_xchacha20poly1305_init_push(key);
	const [state_out, header] = [res.state, res.header];

	console.log('measuring random data encrypt speed');
	let c1 = [];
	let et = process.hrtime()
	for(let i = nIterations - 1; i >= 0; i--) {
		const r = sodium.crypto_secretstream_xchacha20poly1305_push(state_out, data, null,
						i ? sodium.crypto_secretstream_xchacha20poly1305_TAG_MESSAGE : sodium.crypto_secretstream_xchacha20poly1305_TAG_FINAL);
		c1.push(r);
	}
	et = process.hrtime(et);
	console.info('\tencrypted length: %dMB', nIterations);
	console.info('\texecution time: %dms', et[0] * 1000 + et[1] / 1000000)
	console.info('\tencryption speed: %dMB/s', nIterations * 1000 / (et[0] * 1000 + et[1] / 1000000));

	console.log('init stream decryption');
	const state_in = sodium.crypto_secretstream_xchacha20poly1305_init_pull(header, key);
	console.log('measuring random data decrypt speed');
	et = process.hrtime()
	for(let i = 0; i < nIterations; i++) {
		sodium.crypto_secretstream_xchacha20poly1305_pull(state_in, c1[i]);
	}
	et = process.hrtime(et);
	console.info('\texecution time: %dms', et[0] * 1000 + et[1] / 1000000)
	console.info('\tdecryption speed: %dMB/s', nIterations * 1000 / (et[0] * 1000 + et[1] / 1000000));


	// https://libsodium.gitbook.io/doc/public-key_cryptography/authenticated_encryption
	console.log('*** MEASURE PUBLIC KEY ENCRYPTION');

	console.log('generate bob\'s keypair');
	const bob = sodium.crypto_box_keypair();
	console.log('generate alice\'s keypair');
	const alice = sodium.crypto_box_keypair();

	console.log('generate nonce');
	const nonce = sodium.randombytes_buf(sodium.crypto_box_NONCEBYTES);

	console.log('measuring random data encrypt speed');
	let c2 = [];
	et = process.hrtime()
	for(let i = 0; i < nIterations; i++) {
		// bob sends to alice
		const r = sodium.crypto_box_easy(data, nonce, alice.publicKey, bob.privateKey);
		c2.push(r);
	}
	et = process.hrtime(et);
	console.info('\tencrypted length: %dMB', nIterations);
	console.info('\texecution time: %dms', et[0] * 1000 + et[1] / 1000000)
	console.info('\tencryption speed: %dMB/s', nIterations * 1000 / (et[0] * 1000 + et[1] / 1000000));

	console.log('measuring random data decrypt speed');
	et = process.hrtime()
	for(let i = 0; i < nIterations; i++) {
		// alice receives from bob
		sodium.crypto_box_open_easy(c2[i], nonce, bob.publicKey, alice.privateKey);
	}
	et = process.hrtime(et);
	console.info('\texecution time: %dms', et[0] * 1000 + et[1] / 1000000)
	console.info('\tdecryption speed: %dMB/s', nIterations * 1000 / (et[0] * 1000 + et[1] / 1000000));
})();
