### TEST HOWTO

Clone repository to some path:
```
git clone git@bitbucket.org:maxrd2/libsodium-perf.git
```

Install dependencies:
```
cd libsodium-perf
npm install
```

Run Test:
```
npm run test
```

```
> sodi@1.0.0 test
> node index.js

generate random data
*** MEASURE SECRET KEY ENCRYPTION
generate secret key
init stream encryption
measuring random data encrypt speed
        encrypted length: 100MB
        execution time: 1283.367407ms
        encryption speed: 77.9200090750006MB/s
init stream decryption
measuring random data decrypt speed
        execution time: 1296.618334ms
        decryption speed: 77.1236973732318MB/s
*** MEASURE PUBLIC KEY ENCRYPTION
generate bob's keypair
generate alice's keypair
generate nonce
measuring random data encrypt speed
        encrypted length: 100MB
        execution time: 1362.980731ms
        encryption speed: 73.36860875988421MB/s
measuring random data decrypt speed
        execution time: 1356.6133439999999ms
        decryption speed: 73.71297093772358MB/s

```